import React from 'react';
import logo from './logo.svg';
import './App.css';
import Items from './components/items';

export function App() {
  const items = [
    'Shark',
    'Dolphin',
    'Octopus'
  ];
  return (
    <Items items={items}></Items>
  );
}

export default App;
