import React from "react";
import {shallow, mount, render} from 'enzyme';
import renderer from "react-test-renderer";
import Items from "./items";
import {App} from './../App';

test("renders correctly when there are no items", () => {
  const tree = renderer.create(<Items items={[]} />).toJSON();
  expect(tree).toMatchSnapshot();
});

test("check all items are rendered correctly", () => {
  const items = ["Shark", "Dolphin", "Octopus"];
  const tree = renderer.create(<Items items={items} />).toJSON();
  expect(tree).toMatchSnapshot();
});

test('enzyme test', ()=>{
  const wrapper = shallow(<App />);
  expect(wrapper).toMatchSnapshot();
});

test("Mount enzyme Full DOM rendering", () => {
  const wrapper = mount(<App/>);
  expect(wrapper.find("li").first().text()).toBe("Shark");
})

test("Static rendering test", ()=> {
  const wrapper = render(<App/>);
  console.log(wrapper.html());
  expect(wrapper.find("li")).toHaveLength(3);
})