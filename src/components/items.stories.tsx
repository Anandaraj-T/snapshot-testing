import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { Items } from "./items";

export default {
  title: "Example/List",
  component: Items,
} as ComponentMeta<typeof Items>;

const Template: ComponentStory<typeof Items> = (args: any) => (
  <Items {...args} />
);

export const Primary = Template.bind({});
Primary.args = {
  items: ["One", "Two", "Three"],
};
